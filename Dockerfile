FROM amazonlinux as builder
LABEL maintainer="Abel Luck <abel@guardianproject.info>"
ARG NODE_VERSION=8.10.0

RUN yum groupinstall -yq "Development Tools"

# The /var/lang is where AWS installs Node.
#ADD https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}.tar.gz /tmp/
RUN mkdir -p /tmp; \
    curl -vvv https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}.tar.gz | \
    tar -zxvC /tmp/
WORKDIR /tmp/node-v${NODE_VERSION}
RUN mkdir -p /var/lang; \
    ./configure --prefix=/var/lang; \
    make -j $(nproc) all install

FROM amazonlinux
COPY --from=builder /var/lang /var/lang

RUN yum groupinstall -yq "Development Tools"

RUN set -ex; \ 
  groupadd -g 1000 node; \
  useradd -M -d /var/task -s /bin/bash -u 1000 -g 1000 node;

COPY test /
USER node
WORKDIR /var/task
ENV PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/var/lang/bin
ENTRYPOINT ["npm", "install"]

LABEL org.label-schema.build-date="$BUILD_DATE" \
org.label-schema.name="awslambda-npm-install" \
org.label-schema.license="Apache-2.0" \
org.label-schema.description="Native npm module builder for AWS Lambda environments" \
org.label-schema.vcs-type="Git" \
