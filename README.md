# docker-awslambda-npm-install

A docker image that simulates the AWS Lambda environment for node.js. Use this
to to install node modules with native extensions built for the AWS Lambda
environment.

## License

Licensed under the [Apache License 2.0](https://spdx.org/licenses/Apache-2.0.html)

## Author

Created in 2019 by Abel Luck <abel@guardianprojectlinfo> for [the Guardian
Project](https://guardianproject.info).

Based off of original work by [Erica Windisch](https://github.com/ewindisch) at
[iopipe/awslambda-npm-install](https://github.com/iopipe/awslambda-npm-install).
