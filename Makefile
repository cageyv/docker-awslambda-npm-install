BUILD_DATE   ?=$(shell date -u +”%Y-%m-%dT%H:%M:%SZ”)
BUILD_ARGS   ?=
DOCKER_NS    ?= digiresilience
DOCKER_BUILD := sudo docker build --disable-content-trust=false --pull ${BUILD_ARGS} --build-arg BUILD_DATE=${BUILD_DATE}
DOCKER_PUSH  := sudo docker push --disable-content-trust=false
NAMES        := awslambda-npm-install
VERSION      ?= latest

.PHONY: all clean $(NAMES) test

all: $(NAMES) zammad

$(NAMES):
	${DOCKER_BUILD} -t ${DOCKER_NS}/$@:${VERSION} .
	${DOCKER_PUSH} ${DOCKER_NS}/$@:${VERSION}
